﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CJieSuanMain : MonoBehaviour {

    public Text _txtJieSuanInfo;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Counting();

    }

    public void ShowJieSuanInfo()
    {
        _txtJieSuanInfo.text = CPlayerManager.s_Instance.GetJieSuanInfo();
        BeginCount();
    }

    float m_fCount = 0f;
    bool m_bCounting = false;
    void BeginCount()
    {
        m_fCount = 30f;
        m_bCounting = true;
    }

    void Counting()
    {
        if (!m_bCounting)
        {
            return;
        }
        m_fCount -= Time.deltaTime;
        if (m_fCount <= 0)
        {
            EndAll();
        }
    }

    void EndAll()
    {
        PhotonNetwork.Disconnect();
    }

}
