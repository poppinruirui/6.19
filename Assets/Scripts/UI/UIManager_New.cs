﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Runtime.InteropServices;
public class UIManager_New : MonoBehaviour {

    public CanvasScaler _CanvasScaler;

    public Vector3 vecTempPos = new Vector3();
    public Vector3 vecTempPos2 = new Vector3();
    public Vector3 vecTempPos3 = new Vector3();

    // Use this for initialization
    void Start () {
        AdapteIphoneX();

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public GameObject _uiItemSystem;
    public GameObject _uiItemLevel;
    public GameObject _uiMainCtrl;
    public Vector2 m_vecItemSystem_CommonPos = new Vector2(467f, 321f);
    public Vector2 m_vecLevelSystem_CommonPos = new Vector2(1210f, -1485);
    public Vector2 m_vecIPhoneXOffset = new Vector2(85f, 0f);
    public Vector2 m_vecIPhoneXOffset_MainCtrl = new Vector2(-106f, 0f);
    public void AdapteIphoneX()
    {
        vecTempPos = _uiItemSystem.transform.localPosition;
        vecTempPos2 = _uiItemLevel.transform.localPosition;
        vecTempPos3 = _uiMainCtrl.transform.localPosition;
        if (CAdaptiveManager.s_Instance.GetCurDeviceType() == CAdaptiveManager.eDeviceTYpe.iPhone_X)
        {
            vecTempPos.x += m_vecIPhoneXOffset.x;
            vecTempPos2.x += m_vecIPhoneXOffset.x;
            vecTempPos3.x += m_vecIPhoneXOffset_MainCtrl.x;
        }
        else
        {

        }
        _uiItemSystem.transform.localPosition = vecTempPos;
        _uiItemLevel.transform.localPosition = vecTempPos2;
        _uiMainCtrl.transform.localPosition = vecTempPos3;
    }
}
