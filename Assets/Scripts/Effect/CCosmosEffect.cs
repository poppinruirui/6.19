﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCosmosEffect : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

	// 旋转特效
	public SpriteRenderer _srRotationPic;
	bool m_bRotating = false;
	float m_fRotatingAngle = 0f;
	float m_fRotatingSpeed = 0f;

	public void ResetAll()
	{
		m_bRotating = false;
	}

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Rotating ();
	}

    public void SetLocalPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

	public void Rotating( )
	{
		if (!m_bRotating) {
			return;
		}
		m_fRotatingAngle += Time.deltaTime * m_fRotatingSpeed;
		this.transform.localRotation = Quaternion.identity;
		this.transform.Rotate(0.0f, 0.0f, m_fRotatingAngle);
	}

	public void BeginPlayRotation( float fRotatingSpeed )
	{
		m_bRotating = true;
		m_fRotatingSpeed = fRotatingSpeed;
	}
}
