﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CColorManager : MonoBehaviour {

    public static CColorManager s_Instance;

    Dictionary<string, Color> m_dicColors = new Dictionary<string, Color>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Color GetColorByString( string szColor )
    {
        Color color;

        if ( m_dicColors.TryGetValue( szColor, out color) )
        {
            return color;
        }

        szColor = "#" + szColor;
        if (ColorUtility.TryParseHtmlString(szColor, out color))
        {
            m_dicColors[szColor] = color;
            return color;
        }
        return Color.white;
    }
}
