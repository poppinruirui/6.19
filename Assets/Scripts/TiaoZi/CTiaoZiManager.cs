﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTiaoZiManager : MonoBehaviour {

    public static CTiaoZiManager s_Instance = null;

    public Color[] m_aryColors;

    public float m_fTiaoZiCamThreshold_TunShi = 0.12f;
    public float m_fTiaoZiCamThreshold_JinBi = 0.12f;
    public float m_fTiaoZiCamThreshold_ZhanCi_TiJi = 0.12f;
    public float m_fTiaoZiCamThreshold_ZhanCi_JinQian = 0.12f;
    public float m_fTiaoZiCamThreshold_TanKai = 0.12f;
    public float m_fTiaoZiCamThreshold_ShengJi = 0.12f;
    public float m_fTiaoZiCamThreshold_CiMianYi = 0.12f;
    public float m_fTiaoZiCamThreshold_YanMieMianYi = 0.2f;

    // prefab
	public GameObject m_preTiaoZi_Test;
    public GameObject m_preTiaoZi;
    public GameObject m_preTiaoZi_YanMie;
    public GameObject m_preTiaoZi_TunShi;
    public GameObject m_preTiaoZi_JinBi;

    /// <summary>
    /// / type 1: 抛物线
    /// </summary>
    public float m_fPaoWuXianVx0 = 0f;
    public float m_fPaoWuXianVy0 = 0f;
    public float m_fPaoWuXianT = 0f;
    public float m_fPaoWuXianA = 0f;
    public float m_fPaoWuXianBanJingXiShu = 1f;
    public float m_fPaoWuXianSizeXiShu = 0.1f;

    /// <summary>
    /// / type 2: 原地变大，然后一边上升一边变小
    /// </summary>
    public float m_fBianDaT = 0f;
    public float m_fShangShengT = 0f;
    public float m_fBianDaBanJingXiShu = 0f;
    public float m_fShangShengBanJingXiShu = 0f;

    /// <summary>
    /// / type 3: 暴击
    /// </summary>
    public float m_fType3_ScaleXiShu = 0f;
    public float m_fType3_BianDaT = 0f;
    public float m_fType3_BianXiaoT = 0f;
    public float m_fType3_ChiXuT = 0f;
    public float m_fType3_OffsetXiShuX = 0f;
    public float m_fType3_OffsetXiShuY = 0f;

    public float ShengJi_ScaleXiShu = 0f;
    public float ShengJi_BianDaT = 0f;
    public float ShengJi_BianXiaoT = 0f;
    public float ShengJi_ChiXuT = 0f;
    public float ShengJi_OffsetXiShuX = 0f;
    public float ShengJi_OffsetXiShuY = 0f;

    /// <summary>
    /// / type: 湮灭
    /// </summary>
    public float m_fYanMie_ScaleXiShu = 0f;
    public float m_fYanMie_DaWeiYiXiShu = 0.35f;
    public float m_fYanMie_BianDaT = 0f;
    public float m_fYanMie_BianXiaoT = 0f;
    public float m_fYanMie_ChiXuT = 0f;
    public float m_fYanMie_XiaJiangXiShu = 0.25f;
    public float m_fYanMie_BianXiaoEndXiShu = 0.6f;


    /// <summary>
    ///  type：吞噬
    /// </summary>
    public const int MAX_TIAOZI_TUNSHI = 3;
    public float m_fTunShi_ScaleXiShu = 0f;
    public float m_fTunShi_DistanceXiShu = 0f;
    public float m_fTunShi_XiaJiangXiShu = 0f;
    public float m_fTunShi_XiaJiangT = 0.5f;
    public float m_fTunShi_ChiXuT = 0f;
    public float m_fTunShi_DirX = 0f;
    public float m_fTunShi_DirY = 0f;
    public float m_fTunShi_PushOffsetY = 10f;

    /// <summary>
    /// / 金币
    /// </summary>
    public float m_fJinBi_ScaleXiShu = 1f;


    /// <summary>
    /// / 升级
    /// </summary>
    public float m_fShengJi_ChiXuTime = 1f;
    public float m_fShengJi_DirX = -0.5f;
    public float m_fShengJi_DirY = -0.5f;

    /// <summary>
    /// / 刺免疫
    /// </summary>
    public float m_fCiMianYi_ChiXuTime = 1f;


    /// <summary>
    /// / 湮灭免疫
    /// </summary>
    public float m_fYanMieMianYi_ChiXuTime = 1f;

	// 资源类型
	public enum eResourceType
	{
		common,
		jinbi,
	}

	// 轨迹类型
    public enum eTiaoZiType
    {
        none,

        yanmie,  // 湮灭
        tunshi,  // 吞噬
        jinbi,   // 金币

        type_1,  // 普通抛物线
        type_2,  // 原地变大，然后一边上升一边缩小
        type_3,  // 暴击
        type_4,  // 原地展示
    };

    public enum eZiYuanType
    {
        common,
        jinbi,
    };

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
    
       
    }

	// Update is called once per frame
	void Update () {
		
	}

    public CTiaoZi NewTiaoZi(eTiaoZiType eType = eTiaoZiType.none )
    {
        CTiaoZi tiaozi = null;
        if (eType == eTiaoZiType.yanmie)
        {
            if (m_lstTiaoZi_YanMie.Count > 0)
            {
                tiaozi = m_lstTiaoZi_YanMie[0];
                m_lstTiaoZi_YanMie.RemoveAt(0);
                tiaozi.gameObject.SetActive(true);
            }
            else
            {
                tiaozi = GameObject.Instantiate(m_preTiaoZi_YanMie).GetComponent<CTiaoZi>();
            }
        }
        else if (eType == eTiaoZiType.tunshi)
        {
            if (m_lstTiaoZi_TunShi.Count > 0)
            {
                tiaozi = m_lstTiaoZi_TunShi[0];
                m_lstTiaoZi_TunShi.RemoveAt(0);
                tiaozi.gameObject.SetActive(true);
            }
            else
            {
                tiaozi = GameObject.Instantiate(m_preTiaoZi_TunShi).GetComponent<CTiaoZi>();
            }
        }
        else if (eType == eTiaoZiType.jinbi)
        {
            if (m_lstTiaoZi_JinBi.Count > 0)
            {
                tiaozi = m_lstTiaoZi_JinBi[0];
                m_lstTiaoZi_JinBi.RemoveAt(0);
                tiaozi.gameObject.SetActive(true);
            }
            else
            {
                tiaozi = GameObject.Instantiate(m_preTiaoZi_JinBi).GetComponent<CTiaoZi>();
            }
        }
        else
        {
            if (m_lstTiaoZi.Count > 0)
            {
                tiaozi = m_lstTiaoZi[0];
                m_lstTiaoZi.RemoveAt(0);
                tiaozi.gameObject.SetActive( true );
            }
            else
            {
				tiaozi = GameObject.Instantiate(m_preTiaoZi_JinBi).GetComponent<CTiaoZi>();
            }
        }

        tiaozi.SetAlpha(1f);
        tiaozi.SetType(eType);
        tiaozi.gameObject.SetActive(true);
        return tiaozi;
    }

    List<CTiaoZi> m_lstTiaoZi = new List<CTiaoZi>();
    List<CTiaoZi> m_lstTiaoZi_YanMie = new List<CTiaoZi>();
    List<CTiaoZi> m_lstTiaoZi_TunShi = new List<CTiaoZi>();
    List<CTiaoZi> m_lstTiaoZi_JinBi = new List<CTiaoZi>();
    public void  DeleteTiaoZi(CTiaoZi tiaozi)
    {
        //GameObject.Destroy( tiaozi.gameObject );
        eTiaoZiType eType = tiaozi.GetType();
        tiaozi.gameObject.SetActive( false );
        if (eType == eTiaoZiType.yanmie)
        {
            m_lstTiaoZi_YanMie.Add(tiaozi);
        }
        else if (eType == eTiaoZiType.tunshi)
        {
            m_lstTiaoZi_TunShi.Add(tiaozi);
        }
        else if (eType == eTiaoZiType.jinbi)
        {
            m_lstTiaoZi_JinBi.Add(tiaozi);
        }
        else
        {
            m_lstTiaoZi.Add(tiaozi);
        }
    }

    public Color GetColor( int nColorIndex )
    {
        return m_aryColors[nColorIndex];
    }

	List<CTiaoZi> m_lstQueue = new List<CTiaoZi>();
	public struct sTiaoZiQueueItem
	{
		Vector3 pos;
		float fScale;
		Color Color;
	};

	public void AddOneTiaoZi()
	{

	}

}
