﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CLightSystem : MonoBehaviour {

    public enum eLightType
    {
        point,
    };

    public GameObject[] m_aryPrePoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    Dictionary<eLightType, List<CLight>> m_dicRecycledLights = new Dictionary<eLightType, List<CLight>>();
    public CLight NewLight( eLightType type )
    {
        CLight light = null;

        List<CLight> lst = null;
        if ( !m_dicRecycledLights.TryGetValue(type, out lst) )
        {
            lst = new List<CLight>();
            m_dicRecycledLights[type] = lst;
        }
        if ( lst.Count > 0 )
        {
            light = lst[0];
            light.gameObject.SetActive(true);
            lst.RemoveAt( 0 );
            m_dicRecycledLights[type] = lst;
        }
        else
        {
            light = GameObject.Instantiate(m_aryPrePoint[(int)type]).GetComponent<CLight>();
        }
        light.SetType(type);

        return light;
    }

    public void DeleteLight( CLight light )
    {
        eLightType type = light.GetType();
        List<CLight> lst = null;
        if (!m_dicRecycledLights.TryGetValue(type, out lst))
        {
            lst = new List<CLight>();
            m_dicRecycledLights[type] = lst;
        }
        light.gameObject.SetActive( false );
        lst.Add(light);
    }
}
