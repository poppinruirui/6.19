﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBean : MonoBehaviour {

    public SpriteRenderer _sr;
    short m_nGuid = 0;
    string m_szConfigId;
    float m_fDeadTime = 0;
    CMonsterEditor.sThornConfig m_Config;
    CClassEditor.sThornOfThisClassConfig m_ClassConfig;

    bool m_bActive = true;

    public GameObject m_goContainer;
    public Collider2D _Trigger;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSprite( Sprite spr )
    {
        _sr.sprite = spr;
    }

    public void SetColor( Color color )
    {
        _sr.color = color;
    }

	public Color GetColor()
	{
		return _sr.color;
	}

	public Vector3 GetPos()
	{
		return this.transform.position;
	}

    public void SetGuid( short nGuid )
    {
        m_nGuid = nGuid;
    }

    public short GetGuid()
    {
        return m_nGuid;
    }

    public void SetActive( bool bActive )
    {
        m_bActive = bActive;
        m_goContainer.SetActive(bActive);
    }

    public bool GetActive()
    {
        return m_bActive;
    }

    public void SetConfigId( string szConfigId )
    {
        m_szConfigId = szConfigId;

        m_Config = CMonsterEditor.s_Instance.GetMonsterConfigById(m_szConfigId);
    }

    public CMonsterEditor.sThornConfig GetConfig()
    {
        return m_Config;
    }

    public void SetClassConfig(CClassEditor.sThornOfThisClassConfig class_config)
    {
        m_ClassConfig = class_config;
    }

    public CClassEditor.sThornOfThisClassConfig GetClassConfig( )
    {
        return m_ClassConfig;
    }

    public string GetConfigId()
    {
        return m_szConfigId;
    }

    public void SetDeadTime( float fDeadTime )
    {
        m_fDeadTime = fDeadTime;
    }

    public float GetDeadTime()
    {
        return m_fDeadTime;
    }
}
