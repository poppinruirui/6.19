﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CChatMessage : MonoBehaviour {

    public Text _txtContent;
    static Vector2 vecTempSize = new Vector2();
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    float m_fHeight = 0f;
    public void SetSize( float fWidth, float fHeight )
    {
        vecTempSize.x = fWidth;
        vecTempSize.y = fHeight;
        _txtContent.rectTransform.sizeDelta = vecTempSize;
        vecTempPos.x = 0f;
        m_fHeight = fHeight / 2f;
        vecTempPos.y = -m_fHeight;
        vecTempPos.z = 0f;
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        _txtContent.transform.localPosition = vecTempPos;
        _txtContent.transform.localScale = vecTempScale;
    }

    public float GetGeight()
    {
        return m_fHeight;
    }

    public void SetContent( string szContent )
    {
        _txtContent.text = szContent;
    }
}
